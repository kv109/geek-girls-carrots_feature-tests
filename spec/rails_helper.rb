# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require 'spec_helper'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|

  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = false
  config.infer_spec_type_from_file_location!

  #Selenium::WebDriver::Firefox::Binary.path = '/home/kv109/opt/firefox34/firefox-bin'

  module ::Selenium::WebDriver::Firefox
    class Bridge
      attr_accessor :speed

      def execute(*args)
        result = raw_execute(*args)['value']
        case speed
          when :slow
            sleep 0.3
          when :medium
            sleep 0.1
        end
        result
      end
    end
  end

  def set_speed(speed)
    begin
      page.driver.browser.send(:bridge).speed=speed
    rescue
    end
  end

  config.before(:each) do
    # set_speed :slow
  end
end
