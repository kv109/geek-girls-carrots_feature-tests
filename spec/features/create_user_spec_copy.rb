require 'rails_helper'

describe 'Creating User', js: true do

  context 'When I go to welcome page' do

    before do
      visit root_path
      click_link 'New User'
      fill_in 'user_login', with: 'John'
      fill_in 'user_email', with: 'john@example.com'
      click_button 'Create User'
    end

    it 'should display User\'s details page' do
      expect(page).to have_content 'User was successfully created.'

      expect(page).to have_content 'John'
      expect(page).to have_content 'john@example.com'

      visit users_path

      expect(page).to have_content 'John'
      expect(page).to have_content 'john@example.com'
    end
  end
end