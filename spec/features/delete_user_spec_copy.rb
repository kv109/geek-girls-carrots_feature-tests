require 'rails_helper'

describe 'Deleting User', js: true do
  before do
    User.create(login: 'Kacper', email: 'kacper@kacper.kacper')
    visit root_path
    click_link 'Destroy'
  end

  context 'and I confirm dialog' do
    before do
      page.driver.browser.switch_to.alert.accept
    end

    it 'should delete User' do
      expect(page).not_to have_content 'Kacper'
    end
  end

  context 'and I dismiss dialog' do
    before do
      page.driver.browser.switch_to.alert.dismiss
    end

    it 'should not delete User' do
      expect(page).to have_content 'Kacper'
    end
  end
end